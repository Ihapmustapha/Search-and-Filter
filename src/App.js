import React, { Component } from 'react';
import './App.css';
import Searchbar from './components/Searchbar/Searchbar.js';
import Filter from './components/Filter/Filter.js';
import ResultList from './components/ResultList/ResultList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Searchbar /> 
        <Filter />
        <ResultList />
      </div>
    );
  }
}

export default App;
