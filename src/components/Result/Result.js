import React, { Component } from 'react';
import './Result.css'; 

class Result extends Component {

    render() {
        return (
            <div className='result-card'>
                <div className='result-content'>
                    <p className='result-text'>Name: {this.props.name} </p>
                    <p className='result-text'>City: {this.props.city}</p>
                    <p className='result-text'>Street: {this.props.street}</p>
                    <p className='result-text'>Phone: {this.props.phone}</p>
                    <p className='result-text'>Lat: {this.props.lat}</p>
                    <p className='result-text'>Long: {this.props.long}</p>
                </div>
            </div> 
        ); 
    }
}

export default Result; 