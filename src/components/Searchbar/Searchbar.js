import React, { Component } from 'react';
import './Searchbar.css';

//const waitInterval = 3000; 


class Searchbar extends Component {
    constructor() {
        super();
        this.state = {
            search: '', 
        }; 

        this.handleChange = this.handleChange.bind(this); 
        this.sendSearchQuery = this.sendSearchQuery.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);  
    }
    
    render() {

        return (
            <div className='container'>
                <input className='searchInput' 
                    type='text' 
                    placeholder='Search..'
                    ref='search'
                    value={this.state.search}
                    onChange={this.handleChange}
                    onKeyDown={this.handleKeyDown}
                />
            </div> 
        ); 
    }

    handleChange(e) { 
        console.log(this.refs.search.value); 
        this.setState({search: e.target.value});
        this.sendSearchQuery(); 
    }

    sendSearchQuery () { 
        console.log("Search Query Sent");
        //TODO: Managing the frequency requests sent. 
        //setTimeout(() => {
        //     this.sendSearchQuery();
        // }, waitInterval); 
    }

    handleKeyDown(e) {
        if (e.key === 'Enter') {
            e.preventDefault(); 
            console.log("Enter Key is pressed"); 
            this.sendSearchQuery(); 
        }
    }
    
}

export default Searchbar; 